<?php
include 'core/BDD.php';

?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>connexion</title>
</head>
<body>
  <div class="container text-center">
    <div class="col-md-6 col-md-offset-3">
      <form action="login.php" method="post" class="form-signin">
        <h1 class="form-signin-heading text-muted">Se connecter</h1>
        <input type="text" class="form-control" placeholder="Username" name="pseudo" required=""><br>
        <input type="password" class="form-control" placeholder="Password" name="password" required=""><br>
        <hr>
        <button class="btn btn-lg btn-primary btn-block" name="connexion" type="submit">Se connecter</button>
        <h2 class="text-center"><small>Ou</small></h2>
        <button class="btn btn-lg btn-primary btn-block" type="Register"> <a href="register.php"> Créer un compte </a> </button>
      </form>
    </div>
  </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<?php
$username = $_POST["pseudo"];
$password = $_POST["password"];
$error = "";
if(isset($_POST["connexion"])) {
  if (isset($password) && isset($username)) {
    // Les champs sont OK
    $query = "SELECT * FROM user WHERE pseudo='$username'";
    // ON récupère les infos du usezr dans la BDD
    $result = mysqli_query($handle,$query);
    if(mysqli_num_rows($result) ==0){
      $error = "INEXISTANT";
    }
  } else {
    $error = "FORMULAIRE";
  }

  $data = mysqli_fetch_assoc($result);

  if ($error=="")
  if ( $data["password"]==$password) {
  } else {
    $error= "PASSWD";
  }

  if ($error=="")
  if($data["active"] == "1") {
    header('Location:admin.php');
  } else {
    $error = "user";
  }

  switch ($error) {
    case "FORMULAIRE" :
    echo "Tout les champs n'ont pas été remplie, veuillez réessayez";
    break;
    case "BDD";
    echo "Une erreur inattendu c'est produite, veuillez réssayez";
    break;
    case "user";
    header('Location:user.php');
    break;
    echo "Votre compte est désactivé ou à été désactivé par un administrateur pour non respect des régles";
    break;
    case "PASSWD":
    echo "Votre mot de passe est incorrect";
    break;
    case "INEXISTANT":
    echo "Ce compte n'existe pas, veuillez-vous inscrire pour continuer";
    break;
  }
}
?>

</body>
</html>
